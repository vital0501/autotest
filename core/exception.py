class AutotestBaseException(Exception): pass


class NoSuchElementException(AutotestBaseException): pass