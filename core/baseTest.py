import allure
import pytest
import time

from selenium import webdriver
from core.exception import NoSuchElementException


class BaseTest(object):

    driver = webdriver.Firefox()
    driver.maximize_window()

    @staticmethod
    def log_text(log_message):
        with pytest.allure.step(str(log_message)):
            pass

    def log_screenshot(self, name=None, driver=None):
        name = name if name else 'Скриншот'
        allure.attach(self.driver.get_screenshot_as_png(), name, attachment_type=allure.attachment_type.PNG)

    @pytest.yield_fixture(autouse=True)
    def teardown(self):

        yield  # everything after 'yield' is executed on tear-down

        with pytest.allure.step('Закрытие браузера'):
            try:
                self.log_screenshot()
                pass
            except Exception as e:
                self.log_text('Ошибка снятия скриншота')
                self.log_text(str(e))

            try:
                self.driver.quit()
            except Exception as e:
                self.log_text("Ошибка закрытия драйвера")
            time.sleep(2)

    def element_search(self, element: None):
        try:
            element = self.driver.find_element_by_xpath(element)
        except NoSuchElementException:
            pytest.fail("Не удалось найти {0}".format(element))
        element.click()
        time.sleep(2)
        self.log_screenshot()
        return element