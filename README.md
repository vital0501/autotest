## Описание

Проект для автоматического smoke тестирования яндекс маркета(тестовое задание)

---

## Тестовое задание

Автоматизировать следующий сценарий

1. Открыть браузер и развернуть на весь экран.

2. Зайти на yandex.ru.

3. Перейти на Яндекс Маркет

4. Выбрать раздел Компьютеры

5. Выбрать раздел Планшеты

6. Зайти в расширенный поиск

7. Задать параметр поиска от 20000 до 25000 рублей.

8. Выбрать производителя Acer

9. Применить условия поиска

10. Запомнить второй элемент в результатах поиска

11. В поисковую строку ввести запомненное значение.

12. Найти и проверить, что наименование товара соответствует запомненному значению

---

## Структура проекта:

/core/baseTest.py - базовый класс, от которого следует наследоваться при создании автотестов

/core/exception.py - содержит базовые исключения

/core/structure.py - содержит xpath элементов

smoke_test.py - смоук тест, выполняемый файл, согласно технического задания

/report - папка отчетов Allure Report

---

## Локальный запуск

В консольной строке выполнить команду:

C:\Users\User\PycharmProjects\test_home_cb>C:\Users\User\AppData\Local\Programs\Python\Python36-32\Scripts\py.test.exe --alluredir=C:/Users/User/PycharmProjects/test_home_cb/report

---

## Формирование отчета Allure Report

В консолюной строке выполнить команду:

C:\Program Files\allure-2.4.1\bin\allure.bat generate C:\Users\Users\PycharmProjects\test_home_cb\report\smoke_test -o C:\Users\User\PycharmProjects\test_home_cb\report

---

## Поддержка:

В данный момент поддерживается браузер: firefox

Для браузера chrome в файле baseTest.py необходимо добавить строки: 

    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1920,1080")
    driver = webdriver.Chrome(chrome_options=options)
	
Для браузера Internet Explorer:

    driver = webdriver.Ie()
    
---
	
## Конфигурация тестовой машины:

Система:
1. Процессор: Intel(R) Core i3-2100 CPU @3.10GHz 3.10GHz
2. ОЗУ: 8.00 ГБ
3. Тип системы: 64-разрядная операционная система

ПО:
1. OS:              Windows 7 SP1
2. IDE:             PyCharm 2017
3. Язык разработки: Python 3.6.4
4. Отчеты:          Alure report-2.4.1
5. JAVA:            java version "9.0.1"

---
