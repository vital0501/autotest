import allure
import pytest

from core.baseTest import BaseTest
from core.exception import NoSuchElementException
from core.structure import BasePageStructure as Structure


class TestAutotest(BaseTest):

    def test_smoke(self):

        with allure.step("Открываем главную страницу яндекса 'https://www.yandex.ru'"):
            self.driver.get("https://www.yandex.ru/")
            self.log_screenshot()

        with allure.step("Переходим в Маркет"):
            self.element_search(Structure.MARKET_BUTTON)

        with allure.step('Переходим в раздел "Компьютеры"'):
            self.element_search(Structure.COMPUTERS_MENU_ITEM)

        with allure.step('Переходим в раздел "Планшеты"'):
            self.element_search(Structure.PADS_ITEM)

        with allure.step("Задаем минимальную цену: 20000 руб"):
            price_min = self.element_search(Structure.PRICE_MIN)
            price_min.send_keys('20000')

        with allure.step("Задаем максимальную цену: 25000 руб"):
            price_max = self.element_search(Structure.PRICE_MAX)
            price_max.send_keys('25000')

        with allure.step("Разворачиваем список производителей"):
            self.element_search(Structure.SHOW_MORE_BRAND)

        with allure.step("Выбираем производителя"):
            self.element_search(Structure.BRAND_ACER)

        with allure.step("Нажимаем применить"):
            self.element_search(Structure.APPLY_BUTTON)

        with allure.step("Запоминаем второй результат применения фильтра"):
            products = []
            try:
                products = self.driver.find_elements_by_xpath(Structure.PRODUCTS)
            except NoSuchElementException:
                pytest.fail('Не найдены результаты поиска')

            if len(products) > 1:
                second_product = str(products[1].text)
            else:
                pytest.fail('В результате поиска менее двух значений')

        with allure.step("В строку поиска вводим запомненный продукт"):
            searh_field = self.element_search(Structure.SEARCH_FIELD)
            searh_field.clear()
            searh_field.send_keys(second_product)

        with allure.step("Нажимаем кнопку поиска"):
            self.element_search(Structure.SEARCH_BUTTON)

        with allure.step("Запиминаем результаты поиска"):
            try:
                products_new = self.driver.find_elements_by_xpath(Structure.PRODUCTS_CARD)
            except NoSuchElementException:
                pytest.fail('Не найдены результаты поиска')
            searh_result = products_new[0].text

        with allure.step("Обрабатываем результаты поиска"):
            if second_product == searh_result:
                self.log_screenshot()
            else:
                pytest.fail('Не корректный поиск. Ожидаем {0}. Видим {1}'.format(second_product, searh_result))

        self.driver.quit()